import React , { Component } from 'react';
import CourseFrom from './Components/CourseFrom';
import CourseList from './Components/CourseList';

class App extends Component 
{
  state = {
    Courses : [
      {name : "React.js"},
      {name : "ReactNative.js"},
      {name : "Angular.js"},
      {name : "Vue.js"},
    ] ,
    Current : ""
  }

  //Update Course
  updateCourse = (e) => {
    this.setState({
      Current: e.target.value
    })
  }

  //Add Course
  addCourse = (e) => {
    e.preventDefault();
    if (e.target.name.value === '') 
    {
        return false
    }
    else
    {
      let Current = this.state.Current;
      let Courses = this.state.Courses;
      Courses.push({name: Current})
      this.setState({
        Courses,
        Current: ''
      })
    }
  }

  //Delete Course
  deleteCourse = (index) => {
    let Courses = this.state.Courses;
    Courses.splice(index, 1);
    this.setState({
      Courses
    })
  }

  // Edit Course
  editCourse = (index, value) => {
    let Courses = this.state.Courses;
    let Course = Courses[index];
    Course['name'] = value;
    this.setState({
      Courses
    })  
  }

  render()
  {
    const {Courses} = this.state;
    const courseList = Courses.map( (course , index) => {
      return <CourseList details = {course} key = {index} index={index} update={this.handleChange} deleteCourse={this.deleteCourse} editCourse={this.editCourse}/>
    })

    return (
      <section className="App">
        <h2>Add Course</h2>
        <CourseFrom current={this.state.Current} updateCourse={this.updateCourse} addCourse={this.addCourse}/>
        <ul> {courseList} </ul>
      </section>
    );
  }
}

export default App;
